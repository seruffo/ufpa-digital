var title = qsTr('UFPA Digital')

// Layout
var heigth = 640
var width = 480

var mapMarkerColor = '#a80c11'

var maximumFlickVelocity = 5000

// Servers URL
var urlJsonRu = 'https://saest.ufpa.br/ru/ru.cardapio.json.php'
var urlRssNews = 'https://portal.ufpa.br/index.php/ultimas-noticias2?format=feed&type=atom'
var urlRssOpportunity = 'https://portal.ufpa.br/index.php/editais-e-bolsas/155-vagas-estagio?format=feed&type=atom'
var urlRadioWebUFPA = 'http://radio.ufpa.br/'
var urlStreamRadioWebUFPA = 'http://www2.radio.ufpa.br/aovivo'
var hostMqttCircular = 'test.mosquitto.org'
var subMqttCircular = '/ufpa/circular/loc/+'
var portMqttCircular = '1883'
