import QtQuick 2.14
import QtQuick.Controls.Material 2.14

Rectangle {
    color: Material.primary
    Image {
        anchors.fill: parent
        fillMode: Image.PreserveAspectCrop
        source: 'qrc:/static/images/image-background.png'
        opacity: 0.6
    }
}
