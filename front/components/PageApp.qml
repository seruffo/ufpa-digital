import QtQuick 2.14
import QtQuick.Controls 2.14

Item {
    property bool isPortrait: this.width < this.height
    property bool toolButtonMenu: true
    property string title: qsTr("Sem Título")

    anchors.left: parent ? parent.left : undefined
    anchors.leftMargin: drawer.width * drawer.position
}
