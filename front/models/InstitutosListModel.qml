import QtQuick 2.14

ListModel {
    ListElement {
        name: "ICA"
        complement: qsTr("Ciências da Arte")
        phone: "(91) 3241-8369"
        site: "http://ica.ufpa.br"
    }
    ListElement {
        name: "ICB"
        complement: qsTr("Ciências Biológicas")
        phone: "(91)3201-8188"
        email: "icb.ufpa@gmail.com"
        site: "http://icb.ufpa.br/"
    }
    ListElement {
        name: "ICED"
        complement: qsTr("Ciências Educação")
        phone: "(91)3201-7145"
        site: "http://iced.ufpa.br/"
    }
    ListElement {
        name: "ICEN"
        complement: qsTr("Ciências Exatas e Naturais")
        phone: "(91)3201-7103"
        email: "icen@ufpa.br"
        site: "http://icen.ufpa.br"
    }
    ListElement {
        name: "ICJ"
        complement: qsTr("Ciências Jurídicas")
        phone: "(91)3201-7104"
        email: "icj@ufpa.br"
        site: "http://icj.ufpa.br"
    }
    ListElement {
        name: "ICS"
        complement: qsTr("Ciências da Saúde")
        phone: "(91)3201-6808"
        email: "icsgab@ufpa.br"
        site: "http://ics.ufpa.br/"
    }
    ListElement {
        name: "ICSA"
        complement: qsTr("Sociais Aplicadas")
        phone: "(91)3201-7150"
        email: "icsa.se.2018@gmail.com"
        site: "http://icsa.ufpa.br/"
    }
    ListElement {
        name: "IFCH"
        complement: qsTr("Filosofia e Humanas")
        phone: "(91)3201-7146"
        email: "direcaoifch@ufpa.br"
        site: "http://ifch.ufpa.br/"
    }
    ListElement {
        name: "IG"
        complement: qsTr("Geociências")
        phone: "(91)3201-7107"
        email: "dirig@ufpa.br"
        site: "http://ig.ufpa.br/"
    }
    ListElement {
        name: "ILC"
        complement: qsTr("Letras Comunicação")
        phone: "(91)3201-7108"
        email: "ilc@ufpa.br"
        site: "http://ilc.ufpa.br/"
    }
    ListElement {
        name: "ITEC"
        complement: qsTr("Tecnologia")
        phone: "(91)3201-7109"
        site: "http://www.itec.ufpa.br/"
    }
    ListElement {
        name: "IEMCI"
        complement: qsTr("Educação Matemática e Científica")
        phone: "(91)3201-7487"
        email: "ilucena@ufpa.br"
        site: "http://www.iemci.ufpa.br/"
    }
    ListElement {
        name: "IECOS"
        complement: qsTr("Estudos Costeiros")
        phone: "(91)3425-1209"
        email: "iecos@ufpa.br"
        site: "http://iecos.ufpa.br/"
    }
    ListElement {
        name: "IMV"
        complement: qsTr("Medicina Veterinária")
        phone: "(91)3311-4736"
        email: "imv@ufpa.br"
        site: "https://campuscastanhal.ufpa.br/?page_id=1878"
    }
}
