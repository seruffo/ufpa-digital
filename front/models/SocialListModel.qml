import QtQuick 2.14

ListModel {
    ListElement {
        name: "Portal UFPA"
        url: "https://portal.ufpa.br"
        sourceIcon: "qrc:/static/icons/social/site.svg"
    }
    ListElement {
        name: "Facebook"
        url: "https://facebook.com/UFPAOficial"
        sourceIcon: "qrc:/static/icons/social/facebook.svg"
    }
    ListElement {
        name: "Twitter"
        url: "https://twitter.com/ufpa_oficial"
        sourceIcon: "qrc:/static/icons/social/twitter.svg"
    }
    ListElement {
        name: "Instagram"
        url: "https://instagram.com/ufpa_oficial"
        sourceIcon: "qrc:/static/icons/social/instagram.svg"
    }
}
