import QtQuick 2.14

ListModel {
    ListElement {
        name: "NAEA"
        complement: qsTr("Est. Amazônicos")
        phone: "(91)3201-7231"
        email: "secretaria_naea@ufpa.br"
        site: "http://www.naea.ufpa.br/"
    }
    ListElement {
        name: "INEAF"
        complement: qsTr("Agricultura Familiar")
        phone: "(91)3201-8059"
        site: "http://www.ineaf.ufpa.br/"
    }
    ListElement {
        name: "NDAE"
        complement: qsTr("Desenvolvimento Amazônico em Engenharia")
        phone: "(91)3201-7057"
        email: "ndae@ufpa.br"
        site: "http://www.ndae.ufpa.br/"
    }
    ListElement {
        name: "NEAP"
        complement: qsTr("Ecologia Aquática")
        phone: "(91)3201-7178"
        email: "neap@ufpa.br"
        site: "https://www.neap.ufpa.br/"
    }
    ListElement {
        name: "NEB"
        complement: qsTr("Educação Básica")
        phone: "(91)98883-9706"
        email: "neb@ufpa.br"
        site: "https://neb.ufpa.br/"
    }
    ListElement {
        name: "NMT"
        complement: qsTr("Medicina Tropical")
        phone: "(91)3201-0965"
        site: "http://www.nmt.ufpa.br/"
    }
    ListElement {
        name: "NPO"
        complement: qsTr("Oncologia")
        phone: "(91)98107-0858"
        email: "npo.ufpa@gmail.com"
        site: "http://www.npo.ufpa.br/"
    }
    ListElement {
        name: "NTPC"
        complement: qsTr("Comportamento")
        phone: "(91)3201-8536"
        email: "ntpc@ufpa.br"
        site: "http://www.ntpc.ufpa.br/"
    }
    ListElement {
        name: "NUMA"
        complement: qsTr("Meio Ambiente")
        phone: "(91)3201-7652"
        email: "numa@ufpa.br"
        site: "http://www.numa.ufpa.br/"
    }
}
